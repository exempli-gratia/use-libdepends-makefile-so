#!/bin/bash

set -x
rm -rf bin
mkdir bin
make -f Native.Makefile clean
DESTDIR=bin SOMEFILEDESTDIR=somedir libdir=../libdepends-makefile-so/lib includedir=../libdepends-makefile-so/include make -f Native.Makefile install
set +x
