#!/bin/bash

set -x
pushd bin
LD_LIBRARY_PATH=../../libdepends-makefile-so/lib/ ./use-libdepends-makefile-so
popd
set +x
